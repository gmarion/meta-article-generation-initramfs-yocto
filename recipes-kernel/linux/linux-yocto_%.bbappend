FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI += "file://enable-initramfs.cfg"

# Add "bzImage.initramfs" symlink to deploy folder for easy pickup
# by kickstart
kernel_do_deploy_append() {
    for type in ${KERNEL_IMAGETYPES}; do
        if [ -e "$deployDir/${initramfs_base_name}.bin" ]; then
            ln -sf ${initramfs_base_name}.bin $deployDir/${type}.initramfs
        fi
    done
}

